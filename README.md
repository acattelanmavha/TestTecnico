# README #

### What is this repository for? ###

Este repositorio contiene el test técnico

### How do I get set up? ###

La aplicación debe ser desplegada sobre Jboss EAP 7 previa ejecución del archihvo "registrador_datasource.sh" dentro de la carpeta "hsqldb_jboss".
Este script registra el datasource asociado a hsqldb en el servidor Jboss EAP 7.
Se deberá cambiar la ruta definida en el script, por la ruta en el que se encuentre el archivo hsqldb.jar adjunto.