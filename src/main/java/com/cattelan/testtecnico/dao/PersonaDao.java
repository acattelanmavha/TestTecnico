package com.cattelan.testtecnico.dao;

import com.cattelan.testtecnico.modelos.Persona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author alberto
 */

@Stateless
public class PersonaDao {
    
    @PersistenceContext private EntityManager em;
    
    public void Guardar(Persona persona) {
        this.em.persist(persona);
    }
    
    public List<Persona> ListarTodos() {
        Query query = this.em.createQuery("SELECT p FROM Persona p");
        return query.getResultList();
    }
}
