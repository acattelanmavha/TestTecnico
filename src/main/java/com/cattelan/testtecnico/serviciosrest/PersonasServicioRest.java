package com.cattelan.testtecnico.serviciosrest;

import com.cattelan.testtecnico.dao.PersonaDao;
import com.cattelan.testtecnico.modelos.Persona;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alberto
 */
@Path("/personas")
public class PersonasServicioRest {

    @EJB
    private PersonaDao personaDao;

    @GET
    @Path("/listar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> Listar() {
        return this.personaDao.ListarTodos();
    }

    @POST
    @Path("/crear")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response Crear(Persona persona) {
        try {
            this.personaDao.Guardar(persona);
            
            return Response.ok("{\"resultado\" : \"Persona creada ok\" }").build();
        } catch (Exception e) {
            return Response.ok("{\"resultado\" : \" " + e.getMessage() + " \" }").build();
        }
    }
}
